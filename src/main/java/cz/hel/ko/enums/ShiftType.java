package cz.hel.ko.enums;

public enum ShiftType {

	MORNING(1), NIGHT(2), DAY_OFF(3);

	private final int index;

	private ShiftType(int index) {
		this.index = index;
	}

	public int getIndexForType(ShiftType type) {
		return type.index;
	}

	public static ShiftType getType(int id) {
		if (id == 1) {
			return MORNING;
		} else if (id == 2) {
			return NIGHT;
		} else {
			return DAY_OFF;
		}
	}

	public boolean isMorning() {
		return this.equals(MORNING);
	}

	public boolean isNight() {
		return this.equals(NIGHT);
	}

	public boolean isDayOff() {
		return this.equals(DAY_OFF);
	}
	
	public int getIndex() {
		return index;
	}
}
