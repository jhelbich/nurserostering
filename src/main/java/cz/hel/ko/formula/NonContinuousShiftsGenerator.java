package cz.hel.ko.formula;

import java.util.ArrayList;
import java.util.List;

import cz.hel.ko.config.GlobalConfig;
import cz.hel.ko.enums.ShiftType;
import cz.hel.ko.expressions.Expression;
import cz.hel.ko.expressions.NotExpression;
import cz.hel.ko.expressions.OrGroup;
import cz.hel.ko.expressions.Tuple;

public class NonContinuousShiftsGenerator implements FormulaGenerator {

	public List<Expression> generateFormulas() {
		
		List<Expression> resultExpressions = new ArrayList<>();
		for (int i = 1; i <= GlobalConfig.getNumOfNurses(); i++) {
			for (int j = 1; j <= GlobalConfig.getNumOfDays(); j++) {
				
				// not current.isMorning and not current.isNight 
				OrGroup og1 = new OrGroup();
				og1.addExpression(new NotExpression(new Tuple(i, j, ShiftType.MORNING)));
				og1.addExpression(new NotExpression(new Tuple(i, j, ShiftType.NIGHT)));
				resultExpressions.add(og1);
				
				// not current.isNight and not tomorrow.isMorning
				if (j < GlobalConfig.getNumOfDays()) {
					OrGroup og2 = new OrGroup();
					og2.addExpression(new NotExpression(new Tuple(i, j, ShiftType.NIGHT)));
					og2.addExpression(new NotExpression(new Tuple(i, j + 1, ShiftType.MORNING)));
					resultExpressions.add(og2);
				} else {
					OrGroup og3 = new OrGroup();
					og3.addExpression(new NotExpression(new Tuple(i, GlobalConfig.getNumOfDays(), ShiftType.NIGHT)));
					og3.addExpression(new NotExpression(new Tuple(i, 1, ShiftType.MORNING)));
					resultExpressions.add(og3);
				}
			}
		}
		
		return resultExpressions;
	}
	
	public static void main(String[] args) {
		NonContinuousShiftsGenerator g = new NonContinuousShiftsGenerator();
		System.out.println("expressions : " + g.generateFormulas().size());
	}

}
