package cz.hel.ko.formula;

import cz.hel.ko.enums.ShiftType;
import cz.hel.ko.expressions.Expression;
import cz.hel.ko.expressions.NotExpression;
import cz.hel.ko.expressions.Tuple;

class NotClauses implements ClausesStrategy {

	@Override
	public Expression generate(Integer nurseId, Integer dayId, ShiftType type) {
		assert nurseId > 0 && dayId > 0;
		return new NotExpression(new Tuple(nurseId, dayId, type));
	}
	
}