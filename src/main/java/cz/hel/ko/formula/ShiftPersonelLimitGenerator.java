package cz.hel.ko.formula;

import java.util.ArrayList;
import java.util.List;

import org.paukov.combinatorics.Factory;
import org.paukov.combinatorics.Generator;
import org.paukov.combinatorics.ICombinatoricsVector;

import cz.hel.ko.config.GlobalConfig;
import cz.hel.ko.enums.ShiftType;
import cz.hel.ko.expressions.Expression;
import cz.hel.ko.expressions.OrGroup;

/**
 * There is a number of personnel on shift that has to be be met.
 * This number is varying from minimal number of employee to 
 * maximal allowed number of employees. Therefore we have to create
 * min and max clauses for each shift.
 *
 */
public class ShiftPersonelLimitGenerator implements FormulaGenerator {

	public List<Expression> generateFormulas() {
		
		List<Expression> resultExpressions = new ArrayList<>();

		// morning shifts minimal personnel
		int mMinCls = GlobalConfig.getNumOfNurses() - GlobalConfig.getMorningShiftMin() + 1;
		List<ICombinatoricsVector<Integer>> morningMinCombs = getCombinations(mMinCls);
		resultExpressions.addAll(createClauses(morningMinCombs, ShiftType.MORNING, new SimpleClauses()));
		
		// morning shifts maximal personnel
		int mMaxCls = GlobalConfig.getMorningShiftMax() + 1;
		List<ICombinatoricsVector<Integer>> morningMaxCombs = getCombinations(mMaxCls);
		resultExpressions.addAll(createClauses(morningMaxCombs, ShiftType.MORNING, new NotClauses()));
		
		// night shifts minimal personnel
		int nMinCls = GlobalConfig.getNumOfNurses() - GlobalConfig.getNightShiftMin() + 1;
		List<ICombinatoricsVector<Integer>> nightMinCombs = getCombinations(nMinCls);
		resultExpressions.addAll(createClauses(nightMinCombs, ShiftType.NIGHT, new SimpleClauses()));
		
		// night shifts maximal personnel
		int nMaxCls = GlobalConfig.getNightShiftMax() + 1;
		List<ICombinatoricsVector<Integer>> nightMaxCombs = getCombinations(nMaxCls);
		resultExpressions.addAll(createClauses(nightMaxCombs, ShiftType.NIGHT, new NotClauses()));

		return resultExpressions;
	}

	private List<Expression> createClauses (List<ICombinatoricsVector<Integer>> combinations, 
											ShiftType type,
											ClausesStrategy pc) {
		
		List<Expression> expressions = new ArrayList<>();
		for (int j = 1; j <= GlobalConfig.getNumOfDays(); j++) {
			for (ICombinatoricsVector<Integer> combination : combinations) {

				// create clauses
				OrGroup og = new OrGroup();
				expressions.add(og);
				for (Integer nurse : combination.getVector()) {
					og.addExpression(pc.generate(nurse, j, type));
				}
			}
		}
		
		return expressions;
	}

	private List<ICombinatoricsVector<Integer>> getCombinations(int nCombs) {
		Generator<Integer> gen = Factory
				.createSimpleCombinationGenerator(Factory.createVector(getNursesArray()),
						nCombs);
		
		return gen.generateAllObjects();
	}
	
	private Integer[] getNursesArray() {
		Integer[] nurses = new Integer[GlobalConfig.getNumOfNurses()];
		for (int i = 1; i <= GlobalConfig.getNumOfNurses(); i++) {
			nurses[i - 1] = i;
		}
		return nurses;
	}
	
	public static void main(String[] args) {
		ShiftPersonelLimitGenerator g = new ShiftPersonelLimitGenerator();
		System.out.println("expressions : " + g.generateFormulas().size());
	}
	
}
