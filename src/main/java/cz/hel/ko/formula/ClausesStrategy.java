package cz.hel.ko.formula;

import cz.hel.ko.enums.ShiftType;
import cz.hel.ko.expressions.Expression;

/**
 * Strategy for clauses generators.
 *
 */
public interface ClausesStrategy {

	public Expression generate(Integer nurseId, Integer dayId, ShiftType type);
}
