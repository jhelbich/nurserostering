package cz.hel.ko.formula;

import java.util.ArrayList;
import java.util.List;

import org.paukov.combinatorics.Factory;
import org.paukov.combinatorics.Generator;
import org.paukov.combinatorics.ICombinatoricsVector;

import cz.hel.ko.config.GlobalConfig;
import cz.hel.ko.enums.ShiftType;
import cz.hel.ko.expressions.Expression;
import cz.hel.ko.expressions.OrGroup;

/**
 * Each nurse cannot work more than 6 shifts in one week
 * and have more than 3 days off.
 *
 */
public class MaxShiftsInRowGenerator implements FormulaGenerator {

	public List<Expression> generateFormulas() {
		List<Expression> resultExpressions = new ArrayList<>();
		
		int minCs = GlobalConfig.getNumOfDays() - GlobalConfig.getDaysOffMin() + 1;
		int maxCs = GlobalConfig.getDaysOffMax() + 1;
		Generator<Integer> minGen = Factory.createSimpleCombinationGenerator(Factory.createVector(createDaysArray()), minCs);
		Generator<Integer> maxGen = Factory.createSimpleCombinationGenerator(Factory.createVector(createDaysArray()), maxCs);
		
		List<ICombinatoricsVector<Integer>> minCombs = minGen.generateAllObjects();
		List<ICombinatoricsVector<Integer>> maxCombs = maxGen.generateAllObjects();
		
		resultExpressions.addAll(createClauses(minCombs, ShiftType.DAY_OFF, new SimpleClauses()));
		resultExpressions.addAll(createClauses(maxCombs, ShiftType.DAY_OFF, new NotClauses()));
		
		return resultExpressions;
	}

	private List<Expression> createClauses(List<ICombinatoricsVector<Integer>> combinations,
							ShiftType type, ClausesStrategy pc) {

		List<Expression> expressions = new ArrayList<>();
		for (int i = 1; i <= GlobalConfig.getNumOfNurses(); i++) {
			for (ICombinatoricsVector<Integer> combination : combinations) {

				// create clauses
				OrGroup og = new OrGroup();
				expressions.add(og);
				for (Integer day : combination.getVector()) {
					og.addExpression(pc.generate(i, day, type));
				}
			}
		}

		return expressions;
	}
	
	private Integer[] createDaysArray() {
		Integer[] days = new Integer[GlobalConfig.getNumOfDays()];
		for (int i = 1; i <= GlobalConfig.getNumOfDays(); i++) {
			days[i - 1] = i;
		}
		return days;
	}
	
	public static void main(String[] args) {
		MaxShiftsInRowGenerator g = new MaxShiftsInRowGenerator();
		System.out.println("expressions : " + g.generateFormulas().size());
	}
}
