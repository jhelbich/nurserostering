package cz.hel.ko.formula;

import cz.hel.ko.enums.ShiftType;
import cz.hel.ko.expressions.Expression;
import cz.hel.ko.expressions.Tuple;

public class SimpleClauses implements ClausesStrategy {

	@Override
	public Expression generate(Integer nurseId, Integer dayId, ShiftType type) {
		assert nurseId > 0 && dayId > 0;
		return new Tuple(nurseId, dayId, type);
	}

}