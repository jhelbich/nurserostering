package cz.hel.ko.formula;

import java.util.List;

import cz.hel.ko.expressions.Expression;

public interface FormulaGenerator {

	public List<Expression> generateFormulas();
	
}
