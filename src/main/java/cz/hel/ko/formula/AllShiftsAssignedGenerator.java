package cz.hel.ko.formula;

import java.util.ArrayList;
import java.util.List;

import cz.hel.ko.config.GlobalConfig;
import cz.hel.ko.enums.ShiftType;
import cz.hel.ko.expressions.Expression;
import cz.hel.ko.expressions.OrGroup;
import cz.hel.ko.expressions.Tuple;

public class AllShiftsAssignedGenerator implements FormulaGenerator {

	@Override
	public List<Expression> generateFormulas() {
		
		List<Expression> resultExpressions = new ArrayList<>();
		
		for (int i = 1; i <= GlobalConfig.getNumOfNurses(); i++) {
			for (int j = 1; j <= GlobalConfig.getNumOfDays(); j++) {
				
				OrGroup og = new OrGroup();
				resultExpressions.add(og);
				for (ShiftType type : ShiftType.values()) {
					og.addExpression(new Tuple(i, j, type));
				}
			}
		}
		
		return resultExpressions;
	}
	
	

}
