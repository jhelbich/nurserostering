package cz.hel.ko;

import java.util.ArrayList;
import java.util.List;

import org.sat4j.core.VecInt;
import org.sat4j.minisat.SolverFactory;
import org.sat4j.specs.IProblem;
import org.sat4j.specs.ISolver;

import cz.hel.ko.config.GlobalConfig;
import cz.hel.ko.enums.ShiftType;
import cz.hel.ko.expressions.Expression;
import cz.hel.ko.expressions.Tuple;
import cz.hel.ko.formula.AllShiftsAssignedGenerator;
import cz.hel.ko.formula.MaxShiftsInRowGenerator;
import cz.hel.ko.formula.NonContinuousShiftsGenerator;
import cz.hel.ko.formula.ShiftPersonelLimitGenerator;

public class NurseRosteringProblem {

	static final int MAXVAR = 1000000;
	static final int NBCLAUSES = 5000000;
	
	public int[][] createNurseSchedule() throws Exception {

		long start = System.currentTimeMillis();
		List<Expression> expressions = new ArrayList<>();
		expressions.addAll(new AllShiftsAssignedGenerator().generateFormulas());
		expressions.addAll(new ShiftPersonelLimitGenerator().generateFormulas());
		expressions.addAll(new MaxShiftsInRowGenerator().generateFormulas());
		expressions.addAll(new NonContinuousShiftsGenerator().generateFormulas());

		System.out.println("total num of expressions : " + expressions.size() + ", time : " + (System.currentTimeMillis() - start) + "ms");
		
		ISolver solver = SolverFactory.newDefault();
		solver.setTimeout(3600); // 2 minutes timeout
		solver.newVar(MAXVAR);
		solver.setExpectedNumberOfClauses(NBCLAUSES);
		
			
		for (Expression expression : expressions) {
			solver.addClause(new VecInt(expression.evaluate()));
		}
		
		System.out.println("time to vec int creation : " + (System.currentTimeMillis() - start) + "ms");
		
		IProblem problem = solver;
		
		int[][] scheduleMatrix = null;
		if (problem.isSatisfiable()) {
			System.out.println(" Satisfiable ! ");
			scheduleMatrix = fillScheduleMatrix(getModelInTuples(problem.model()));
		} else {
			System.out.println(" Unsatisfiable ! ");
		}
		System.out.println("total time : " + (System.currentTimeMillis() - start) + "ms");
		
		if (scheduleMatrix == null) {
			throw new UnsatisfiableException();
		}
		
		return scheduleMatrix;
	}

	private int[][] fillScheduleMatrix(List<Tuple> modelTuples) {
		int[][] scheduleMatrix = new int[GlobalConfig.getNumOfNurses()][GlobalConfig.getNumOfDays()];
		for (Tuple tuple : modelTuples) {
			scheduleMatrix[tuple.getNurseId() - 1][tuple.getDayId() - 1] = tuple.getShift().getIndex();
		}
		
		return scheduleMatrix;
	}

	private List<Tuple> getModelInTuples(int[] model) {
		List<Tuple> modelTuples = new ArrayList<>();
		for (int clause : model) {
			Tuple t = decodeClause(clause);
			if (t != null) {
				modelTuples.add(t);
			}
		}
		return modelTuples;
	}
	
	private Tuple decodeClause(int clause) {
		if (clause <= 0) {
			return null;
		}
		int nurseId = clause / 1000;
		int dayId = (clause % 1000) / 10;
		ShiftType shift = ShiftType.getType(clause % 10);
		
		return new Tuple(nurseId, dayId, shift);
	}
	
	public class UnsatisfiableException extends Exception {

		private static final long serialVersionUID = 1L;
		
	}

	public static void main(String[] args) throws Exception {
		
		long startMem = (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / (1024*1024);
		
		try {
			int[][] scheduleMatrix = new NurseRosteringProblem().createNurseSchedule();
			
			printScheduleMatrix(scheduleMatrix);
			
		} catch (UnsatisfiableException e) {
			System.out.println("Nurse shift roster is unsatisfiable,"
					+ " please change shift conditions.");
		}
		
		long endMem = (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / (1024*1024);;
		
		System.out.println("memory : " + (endMem - startMem) + " MB");
		
	}

	private static void printScheduleMatrix(int[][] scheduleMatrix) {
		for (int i = 0; i < scheduleMatrix.length; i++) {
			StringBuilder sb = new StringBuilder();
			for (int j = 0; j < scheduleMatrix[i].length; j++) {
//				sb.append(ShiftType.getType(scheduleMatrix[i][j])).append(", ");
				sb.append(scheduleMatrix[i][j]).append(", ");
			}
			sb.delete(sb.length() - 2, sb.length());
			System.out.println(sb.toString());
		}
	}

}


