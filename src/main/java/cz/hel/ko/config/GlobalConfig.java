package cz.hel.ko.config;

public class GlobalConfig {

	private static final int NUM_OF_DAYS = 7;
	private static final int NUM_OF_NURSES = 15;
	private static final int MORNING_SHIFT_MIN = 4;
	private static final int MORNING_SHIFT_MAX = 6;
	private static final int NIGHT_SHIFT_MIN = 3;
	private static final int NIGHT_SHIFT_MAX = 5;
	private static final int DAYS_OFF_MIN = 1;
	private static final int DAYS_OFF_MAX = 5;

	private static final int SHIFT_VAR_LENGHT = 1;
	private static final int DAYS_VAR_LENGHT = 2;
	private static final int NURSES_VAR_LENGHT = 2;

	public static int getNumOfDays() {
		return NUM_OF_DAYS;
	}

	public static int getNumOfNurses() {
		return NUM_OF_NURSES;
	}

	public static int getMorningShiftMin() {
		return MORNING_SHIFT_MIN;
	}

	public static int getMorningShiftMax() {
		return MORNING_SHIFT_MAX;
	}

	public static int getNightShiftMin() {
		return NIGHT_SHIFT_MIN;
	}

	public static int getNightShiftMax() {
		return NIGHT_SHIFT_MAX;
	}

	public static int getDaysOffMax() {
		return DAYS_OFF_MAX;
	}

	public static int getDaysOffMin() {
		return DAYS_OFF_MIN;
	}

	public static int getShiftVarLenght() {
		return SHIFT_VAR_LENGHT;
	}

	public static int getDaysVarLenght() {
		return DAYS_VAR_LENGHT;
	}

	public static int getNursesVarLenght() {
		return NURSES_VAR_LENGHT;
	}

}
