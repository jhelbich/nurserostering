package cz.hel.ko.expressions;

import java.util.Arrays;

import cz.hel.ko.enums.ShiftType;

public class Tuple implements Expression {

	private int nurseId;
	private int dayId;
	private ShiftType shift;

	public Tuple(int nurseId, int dayId, ShiftType shift) {
		assert nurseId > 0 && dayId > 0;
		
		this.nurseId = nurseId;
		this.dayId = dayId;
		this.shift = shift;
	}

	@Override
	public int[] evaluate() {
		int nid = nurseId * 1000;
		int did = dayId * 10;

		int result = nid + did + shift.getIndex();
		assert result > 1000;
		return new int[] { result };
	}

	public int getNurseId() {
		return nurseId;
	}

	public void setNurseId(int nurseId) {
		this.nurseId = nurseId;
	}

	public int getDayId() {
		return dayId;
	}

	public void setDayId(int dayId) {
		this.dayId = dayId;
	}

	public ShiftType getShift() {
		return shift;
	}

	public void setShift(ShiftType shift) {
		this.shift = shift;
	}

	public static void main(String[] args) {
		Tuple t = new Tuple(1, 2, ShiftType.MORNING);
		System.out.println(Arrays.toString(t.evaluate()));
	}

}