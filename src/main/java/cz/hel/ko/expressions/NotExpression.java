package cz.hel.ko.expressions;

public class NotExpression implements Expression {

	private final Expression expression;

	public NotExpression(Expression expr) {
		this.expression = expr;
	}

	@Override
	public int[] evaluate() {
		int[] value = expression.evaluate();
		for (int i = 0; i < value.length; i++) {
			value[i] = value[i] * -1;
		}

		return value;
	}

}
