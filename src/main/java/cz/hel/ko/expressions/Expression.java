package cz.hel.ko.expressions;


public interface Expression {

	public int[] evaluate();

}
