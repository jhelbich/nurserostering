package cz.hel.ko.expressions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import cz.hel.ko.enums.ShiftType;

public class OrGroup implements Expression {

	private final List<Expression> expressions = new ArrayList<>();

	public void addExpression(Expression expr) {
		expressions.add(expr);
	}

	@Override
	public int[] evaluate() {
		List<Integer> temp = new ArrayList<>();
		
		for (Expression expression : expressions) {
			int[] evaluated = expression.evaluate();
			for (int eval : evaluated) {
				temp.add(eval);
			}
		}
		
		int[] result = new int[temp.size()];
		for (int i = 0; i < temp.size(); i++) {
			result[i] = temp.get(i);
		}
		
		return result;
	}
	
	public static void main(String[] args) {
		OrGroup og = new OrGroup();
		og.addExpression(new Tuple(1, 2, ShiftType.MORNING));
		og.addExpression(new NotExpression(new Tuple(2, 3, ShiftType.NIGHT)));
		
		System.out.println(Arrays.toString(og.evaluate()));
	}
	
}
